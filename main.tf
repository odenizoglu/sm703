provider "kubernetes" {
  config_path    = "C:/Users/odeni/.kube/config"
  config_context_cluster = "minikube"
}

provider "helm" {
  kubernetes{
    config_path    = "C:/Users/odeni/.kube/config"
    config_context_cluster = "minikube"
  }
  
}

resource "kubernetes_namespace" "argocd_namespace" {
  metadata {
    name = "argocd"
  }
}

resource "kubernetes_namespace" "production_namespace" {
  metadata {
    name = "production"
  }
}

resource "kubernetes_namespace" "development_namespace" {
  metadata {
    name = "development"
  }
}

resource "kubernetes_namespace" "kubernetes_namespace_namespace" {
  metadata {
    name = "qa"
  }
}

resource "helm_release" "local" {
    name        = "argo-cd"
    chart       = "./argo-cd"
    namespace = "argocd"
}
