package com.example.springboot;

import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("calculator")
public class BasicController {

	@RequestMapping(path = "/add/{integer1}/{integer2}", method = RequestMethod.GET)
	public int getBook(@PathVariable int integer1, @PathVariable int integer2) {
		return integer1 + integer2;
	}
}
