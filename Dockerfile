FROM openjdk:8-jdk-alpine
ARG JAR_FILE=target/*.jar
EXPOSE 8080
COPY ${JAR_FILE} /opt
WORKDIR /opt
CMD ["java","-jar","/opt/spring-boot-complete-0.0.1-SNAPSHOT.jar"]

